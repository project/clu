<?php

namespace Drupal\clu\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Form to List Currently Logged-in Users.
 */
class ListCurrentlyLoggedinUsers extends FormBase {

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The current Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * Constructs a new Get User Session.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   A Database connection to use for reading and writing database data.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   */
  public function __construct(Connection $database, Request $request, MessengerInterface $messenger, AccountInterface $account, PagerManagerInterface $pager_manager) {
    $this->database = $database;
    $this->request = $request;
    $this->messenger = $messenger;
    $this->currentUser = $account;
    $this->pagerManager = $pager_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('messenger'),
      $container->get('current_user'),
      $container->get('pager.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'list_currently_logged_in_users';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $search_value = $this->request->query->get('search_user');
    $form['clu'] = [
      '#type' => 'fieldset',
      '#title' => t('Search'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['clu']['search_user'] = [
      '#type' => 'textfield',
      '#title' => 'Search by Username',
      '#default_value' => $search_value,
    ];
    $form['clu']['search_submit'] = [
      '#type' => 'submit',
      '#value' => 'Search',
    ];
    $form['clu']['clear_submit'] = [
      '#type' => 'submit',
      '#value' => 'Clear',
    ];
    $header = [
      'user' => t('User'),
      'loggedin_since' => t('Loggedin Since'),
      'session' => t('Session Details'),
      'action' => t('OPERATIONS'),
    ];

    $num_per_page = 10;
    $page = $this->pagerManager->findPage();

    $query = $this->database->select('sessions', 's');
    $query->join('users_field_data', 'u', 's.uid = u.uid');
    $query->fields('u', ['uid']);
    $query->fields('u', ['name']);
    $query->fields('u', ['mail']);
    $query->fields('s', ['timestamp']);
    $query->fields('s', ['hostname']);
    $query->fields('s', ['sid']);
    $query->orderBy('uid', 'DESC');
    $query->distinct();

    if (isset($search_value) && !empty($search_value)) {
      $query->condition('u.name', $this->database->escapeLike($search_value) . '%', 'LIKE');
    }

    $pager_query = $query;
    $pager_query = $pager_query->execute()->fetchAll();
    $pager_total = count($pager_query);
    $offset = $num_per_page * $page;
    if ($pager_total >= $num_per_page) {
      $this->pagerManager->createPager($pager_total, $num_per_page, $element = 0);
      $query->range($offset, $num_per_page);
    }
    $results = $query->execute()->fetchAll();
    $output = [];
    foreach ($results as $result) {
      if ($result->uid == 0) {
        continue;
      }
      if ($this->currentUser->id() != $result->uid) {
        $url = Url::fromUserInput('/admin/people/clu/' . $result->uid);
        $url->setOptions(['attributes' => ['class' => ['button']]]);
        $action = Link::fromTextAndUrl(t('End session'), $url);
      }
      else {
        $url = Url::fromUserInput('/user/logout');
        $url->setOptions(['attributes' => ['class' => ['button']]]);
        $action = Link::fromTextAndUrl(t('Logout'), $url);
      }
      $session_url = Url::fromUserInput('/admin/people/clu/session/' . $result->sid);
      $session_url->setOptions(['attributes' => ['class' => ['button']]]);
      $output[$result->uid] = [
        'user' => $result->name . ' (' . $result->mail . ')',
        'loggedin_since' => $this->getLastLoginTime($result->timestamp),
        'session' => Link::fromTextAndUrl(t('Session Details'), $session_url),
        'action' => $action,
      ];
    }
    if (!empty($output)) {
      $form['table'] = [
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $output,
        '#empty' => t('No logged-in users found'),
      ];
      $form['pager'] = [
        '#type' => 'pager',
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => 'End sessions',
        '#button_type' => 'primary',
      ];
    }
    else {
      $form['table'] = [
        '#type' => 'tableselect',
        '#header' => [],
        '#options' => [],
        '#empty' => t('No logged-in users found'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('op') == 'End sessions') {
      $selected_users = $this->getSelectedUsers($form_state->getValue('table'));
      if (!$selected_users) {
        $form_state->setErrorByName('clu_table', $this->t('Please select atleast one user to end the session.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operation = $form_state->getValue('op');
    if ($operation == 'Search') {
      $this->request->query->set('search_user', $form_state->getValue('search_user'));
    }
    if ($operation == 'End sessions') {
      $selected_users = $this->getSelectedUsers($form_state->getValue('table'));
      foreach ($selected_users as $delete_session_uid) {
        $user = User::load($delete_session_uid);
        if ($this->currentUser->id() != $delete_session_uid && $delete_session_uid != 0) {
          $this->database->delete('sessions')
            ->condition('uid', $delete_session_uid)
            ->execute();
          $this->messenger->addMessage(t('@username (@userid) user session has been ended.', [
            '@username' => $user->getAccountName(),
            '@userid' => $user->id(),
          ]));
        }
      }
    }
    if ($operation == 'Clear') {
      $form_state->setRedirect('clu.list');
    }
  }

  /**
   * Callback function for getting last login time.
   *
   * @param string $time
   *   The timestamp.
   *
   * @return string
   *   The logged in time.
   */
  protected function getLastLoginTime($time) {
    $second = 1;
    $minute = 60 * $second;
    $hour = 60 * $minute;
    $day = 24 * 60 * 60 * 1;
    $difference = time() - $time;
    $days = !empty(floor($difference / $day)) ? floor($difference / $day) . ' days ' : '';
    $hours = !empty(floor(($difference % $day) / $hour)) ? floor(($difference % $day) / $hour) . ' hours ' : '';
    $minutes = !empty(floor((($difference % $day) % $hour) / $minute)) ? floor((($difference % $day) % $hour) / $minute) . ' minutes ' : '';
    $seconds = !empty(floor(((($difference % $day) % $hour) % $minute) / $second)) ? floor(((($difference % $day) % $hour) % $minute) / $second) . ' seconds' : '';
    return $days . $hours . $minutes . $seconds;
  }

  /**
   * Callback function for getting last login time.
   *
   * @param array $users
   *   The selected users from table.
   *
   * @return array
   *   The selected users from the table.
   */
  protected function getSelectedUsers($users) {
    $selected_users = [];
    foreach ($users as $user_id => $value) {
      if ($value != 0) {
        $selected_users[$user_id] = $value;
      }
    }
    if (empty($selected_users)) {
      return FALSE;
    }
    return $selected_users;
  }

}
