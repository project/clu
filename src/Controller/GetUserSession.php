<?php

namespace Drupal\clu\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller to Get User Session.
 *
 * @package Drupal\clu\Controller
 */
class GetUserSession extends ControllerBase {

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new Get User Session.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   A Database connection to use for reading and writing database data.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
    );
  }

  /**
   * GetUserSessionDetail.
   *
   * @param string $session_id
   *   A session id.
   *
   * @return array
   *   Return the output of ended user session.
   */
  public function getUserSessionDetail($session_id) {
    $query = $this->database->select('sessions', 's');
    $query->join('users_field_data', 'u', 's.uid = u.uid');
    $query->condition('s.sid', $session_id, '=');
    $query->fields('u', ['name']);
    $query->fields('u', ['mail']);
    $query->fields('s', ['uid']);
    $query->fields('s', ['sid']);
    $query->fields('s', ['timestamp']);
    $query->fields('s', ['hostname']);
    $query->fields('s', ['session']);
    $results = $query->execute()->fetchAll();

    foreach ($results as $result) {
      $output['clu']['session'] = [
        '#type' => 'fieldset',
        '#title' => t('Session Details'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      ];
      $output['clu']['session']['name'] = [
        '#type' => 'markup',
        '#markup' => 'User Name : ' . $result->name . ' </br>',
      ];
      $output['clu']['session']['mail'] = [
        '#type' => 'markup',
        '#markup' => 'Mail : ' . $result->mail . ' </br>',
      ];
      $output['clu']['session']['uid'] = [
        '#type' => 'markup',
        '#markup' => 'Uid : ' . $result->uid . ' </br>',
      ];
      $output['clu']['session']['sid'] = [
        '#type' => 'markup',
        '#markup' => 'Sid : ' . $result->sid . ' </br>',
      ];
      $output['clu']['session']['timestamp'] = [
        '#type' => 'markup',
        '#markup' => 'Timestamp : ' . $result->timestamp . ' </br>',
      ];
      $output['clu']['session']['hostname'] = [
        '#type' => 'markup',
        '#markup' => 'Hostname : ' . $result->hostname . ' </br>',
      ];
      $output['clu']['session']['session_data'] = [
        '#type' => 'markup',
        '#markup' => 'Session : ' . $result->session . ' </br>',
      ];
    }
    $output['clu']['back'] = [
      '#type' => 'link',
      '#title' => $this->t('Back'),
      '#attributes' => [
        'class' => ['button', 'button--primary'],
      ],
      '#url' => Url::fromRoute('clu.list'),
    ];

    return $output;
  }

}
