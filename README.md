# Currently Logged-in Users
This module will provide the list of users who are currently logged in the site, and admin will have an option to end the users session.

#### Features covered
1. List of currently logged-in users across the site.
2. Admin users have the capability to end the user sessions & see the session details.
3. Admin users who have certain permissions can only do end the user sessions.

## Installation
Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration
1. After enabling the module, navigate to the permissions page `/admin/people/permissions/module/clu`, and assign the role users who can access the currently logged-in users.
2. Navigate to the **List all Currently logged-in users** `/admin/people/clu`, you can see the list of logged-in users across the website.
3. You can search for the users by username by using the filter **Search by Username**.
4. You can see the session details of a logged-in user by clicking on **Session Details**.
5. You can end the session of a logged-in user by clicking on **End Session** and You can also select multiple users and end the sessions.
